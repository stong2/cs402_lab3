         .data 0x10010000
m1: .asciiz "Please enter two integer number: "
m2: .asciiz "I am far away"
m3: .asciiz "I am nearby"
     
      .text
      .globl main
main: addu $s0, $ra, $0 # save $31 in $16

      li $v0, 4         # system call for print_str
      la $a0, m1      # address of string to print
      syscall

      li $v0, 5         # system call for read_int
      syscall
      addu $t0, $v0, $0 # first integer in $t0
      li $v0, 5         # system call for read_int
      syscall
      addu $t1, $v0, $0    #second integer in $t1

# comparision 
      beq $t0, $t1, Far # if two input are equal than jump to Far
      li $v0, 4         # system call for print_string
      la $a0, m3
      syscall
      j Exit

   .text 0x00410000     #far away address
Far:  li $v0, 4
      la $a0, msg4
      syscall
      j Exit
      
# restore now the return address in $ra and return from main
Exit: addu $ra, $0, $s0    # return address back in $31
      jr $ra            # return from main