         .data 0x10010000
var1:    .word 5 
limit:   .word 100

  
         .text
         .globl main
main:
         lw $a0, var1 # load var1 to t0
         lw $a1, limit # load limit to a1
         move $t0, $a0 # i is in $t0
         Loop: ble $a1, $t0, Exit # exit if limit <= i
         # body of the for loop
         addi $a0, $a0, 1 # var = var + 1
         addi $t0, $t0, 1 # i = i+1
         j Loop
Exit: 
sw $a0, var1