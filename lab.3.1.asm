         .data 0x10010000
var1:    .word 5 
var2:    .word 7
var3:    .word -2019

  
         .text
         .globl main
main:
         lw $t0, var1 # load var1 to t0
         lw $t1, var2 # load var2 to t1
         lw $t2, var3 # load var3 to t2
         bne $t0, $t1, Else # go to Else if $t0 != $t1
         # code for block #1
         sw $t2, var1
         sw $t2, var2
         beq $0, $0, Exit # go to Exit (skip code for block #2)
         Else:
         # code for block #2
         sw $t1 var1
         sw $t0 var2
         Exit:
         