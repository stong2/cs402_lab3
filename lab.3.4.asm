         .data 0x10010000
i:    .word 0
var_j:    .word 5
limit:    .word 10
my_array: .space 40 # element size is a word


  
         .text
         .globl main
main:
         lw $a0, i # load i to a0
         lw $a1, var_j # load j to a1
         lw $a2, limit # load limit to a2
         la $a3, my_array # load array address into a2 
         #sw $a1, a3
         move $t0, $a0 # i is in $t0
         Loop: ble $a2, $t0, Exit # exit if limit <= i
         # body of the for loop
         
         sw $a1, 0($a3) # put the j into my_array[i]
         addi $a1, $a1, 1 # j = j + 1
         addi $t0, $t0, 1 # i = i+1
         addi $a3, $a3, 4  # update the address of array
         j Loop
Exit: 
